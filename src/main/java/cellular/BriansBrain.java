package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns){
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }


    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++){
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
        currentGeneration = nextGeneration;
        
    }

    @Override
    public CellState getNextCell(int row, int col) {
        // En levende celle blir døende
        //En døende celle blir død
        //En død celle med akkurat 2 levende naboer blir levende
        //En død celle forblir død ellers
        CellState newState = CellState.DEAD;
        int livingNeighbors = countNeighbors(row, col, CellState.ALIVE);
        CellState originalState = getCellState(row, col);

        if(originalState.equals(CellState.DEAD)){
            if(livingNeighbors == 2){
                newState = CellState.ALIVE;
            }
        }
        if(originalState.equals(CellState.ALIVE)){
            newState = CellState.DYING;
        }
        
        return newState;
    }

    private int countNeighbors(int row, int col, CellState state) {
		int count = 0;

		for(int dx = row-1; dx <= row+1 ; dx++){
			for(int dy =col-1; dy <= col+1; dy++){
				// check out of bounds
				if(dx < 0 || dy < 0 || dy >= numberOfColumns() || dx >= numberOfRows()){
					continue;
				}
				// check if it is the OG
				if(dx == row && dy == col){
					continue;
				}
				if (getCellState(dx, dy).equals(state)){
					count++;
				}
			}
		}

		return count;
	}

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    
}
