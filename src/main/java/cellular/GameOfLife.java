package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
	
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
	
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++){
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState newState = CellState.ALIVE;

		int livingNeighbors = countNeighbors(row, col, CellState.ALIVE);
		CellState originalState = getCellState(row, col);
		
			if(originalState.equals(CellState.ALIVE)){
				if(livingNeighbors < 2 || livingNeighbors > 3){
				newState = CellState.DEAD;
				}
			}
		
		else if(livingNeighbors != 3) {
			newState = CellState.DEAD;
		}
		
		return newState;
	}

	
	private int countNeighbors(int row, int col, CellState state) {
		int count = 0;

		for(int dx = row-1; dx <= row+1 ; dx++){
			for(int dy =col-1; dy <= col+1; dy++){
				// check out of bounds
				if(dx < 0 || dy < 0 || dy >= numberOfColumns() || dx >= numberOfRows()){
					continue;
				}
				// check if it is the OG
				if(dx == row && dy == col){
					continue;
				}
				if (getCellState(dx, dy).equals(state)){
					count++;
				}
			}
		}

		return count;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
