package datastructure;


import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][] grid;
    

    public CellGrid(int rows, int columns, CellState initialState) {
		
        this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];
        for (int row = 0; row < rows; row++){
            for(int col = 0; col < columns; col++) {
                grid[row][col] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        
        return this.rows;
    }

    @Override
    public int numColumns() {
    
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        
        if(row < 0 || column < 0 || row >= numRows() || column >= numColumns()){
            throw new IndexOutOfBoundsException();
        }
        
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        
        if(row < 0 || column < 0 || row >= numRows() || column >= numColumns() ){
            throw new IndexOutOfBoundsException("Feil størrelse");
        }
        else{
            return grid[row][column];
        }
        
    }
    
    
    @Override
    public IGrid copy() {
        IGrid newGrid = new CellGrid(rows, columns, null);

        for (int row = 0; row < rows; row++){
            for(int col = 0; col < columns; col++) {
                newGrid.set(row, col, this.get(row, col));
            }
        }
        return newGrid;
    }
    
}
